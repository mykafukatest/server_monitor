package com.lbl.server_monitor.unettty;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.lbl.server_monitor.netty.MessageProtocol;
import com.lbl.server_monitor.udp.Utils;
import com.lbl.server_monitor.utils.SpringUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;


/**
 * 处理业务的handler
 */
@Slf4j
public class UMyServerHandler extends SimpleChannelInboundHandler<MessageProtocol> implements Runnable{
    private static  String cleanSize = SpringUtil.getProperties("monitor.client.block.queue.clean.size");

    private int count;
    private static ChannelGroup clients=new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        //建立连接
        log.info("建立连接 u client ip:{}",ctx.channel().remoteAddress());
        clients.add(ctx.channel());
    }
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageProtocol msg) throws Exception {
        //转发到视图tcp
        this.putIntoBlockingQueue(ctx,msg);
    }
    /**
     * 接收的数据放入阻塞队列
     */
    private void putIntoBlockingQueue(ChannelHandlerContext ctx, MessageProtocol msg) throws InterruptedException {
        BlockingQueue<ByteBuffer> queue = SpringUtil.getBean("byteBuffersBlockingQueue", BlockingQueue.class);
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(msg.getLen());
        byteBuffer.put(msg.getContent(),0,msg.getLen());
        //清除太久的数据
        if(queue.size()>Integer.parseInt(cleanSize)){
            queue.clear();
        }
        //放入阻塞队列
        queue.put(byteBuffer);
    }
    @Override
    public void run() {
        BlockingQueue<MessageProtocol> queue = SpringUtil.getBean("messageProtocolBlockingQueue", BlockingQueue.class);
        while (true) {
            try {
                //转发报文
                MessageProtocol take = queue.take();
                for (Channel client : clients) {
                    client.writeAndFlush(take);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
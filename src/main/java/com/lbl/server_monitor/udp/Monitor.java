package com.lbl.server_monitor.udp;

import com.lbl.server_monitor.netty.MyServer;
import com.lbl.server_monitor.unettty.UMyServer;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * @author lbl
 */
@Slf4j
public class Monitor  implements Runnable{

	/**
	 * upd封装
	 */
	private ReceivePacketBuilder builder;


	public Monitor(int udpPort){
		builder=new ReceivePacketBuilder(udpPort);
	}

	public static void startModel1(int udpPort,int tcpPort) {
		Monitor monitor = new Monitor(udpPort);
		Thread thread=new Thread(monitor);
		thread.start();
		new Thread(()->{
			try {
				startNetty(tcpPort);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}

	public static void startModel2(int uTcpPort,int tcpPort) {
		new Thread(()->{
			try {
				startUNetty(uTcpPort);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		new Thread(()->{
			try {
				startNetty(tcpPort);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}


	public static void startNetty(int tcpPort) throws Exception {
		MyServer.start(tcpPort);
	};
	public static void startUNetty(int uTcpPort) throws Exception {
		UMyServer.start(uTcpPort);
	};
	@Override
	public void run() {
		HashMap<Integer, UnitFrame> map=new HashMap<>();
		long currentId=0;
		while(true) {
			//接收报文放入阻塞队列
			builder.forwardToTcp();
		}
	}

}

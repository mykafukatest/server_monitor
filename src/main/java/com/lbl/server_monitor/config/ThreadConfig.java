package com.lbl.server_monitor.config;

import com.lbl.server_monitor.netty.MessageProtocol;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/11 9:08
 */
@Configuration
public class ThreadConfig {
    @Bean
    public BlockingQueue<ByteBuffer> byteBuffersBlockingQueue(){
        return new LinkedBlockingQueue<>();
    }
    @Bean
    public BlockingQueue<MessageProtocol> messageProtocolBlockingQueue(){
        return new LinkedBlockingQueue<>();
    }
    @Bean
    public Map<String,List<String>> monitorMap(){
        return new HashMap<>();
    }

}

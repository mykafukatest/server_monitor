package com.lbl.server_monitor.unettty;

import com.lbl.server_monitor.netty.MyMessageDecoder;
import com.lbl.server_monitor.netty.MyMessageEncoder;
import com.lbl.server_monitor.netty.MyServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;


public class UMyServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        UMyServerHandler myServerHandler = new UMyServerHandler();
        new Thread(myServerHandler).start();
        pipeline.addLast(new UMyMessageDecoder());//解码器
        pipeline.addLast(new UMyMessageEncoder());//编码器
        pipeline.addLast(myServerHandler);
    }
}
package com.lbl.server_monitor.netty;

import com.lbl.server_monitor.udp.Utils;
import com.lbl.server_monitor.utils.SpringUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;


/**
 * 处理业务的handler
 */
@Slf4j
public class MyServerHandler extends SimpleChannelInboundHandler<MessageProtocol> implements Runnable{
    private static  String cleanSize = SpringUtil.getProperties("monitor.client.block.queue.clean.size");

    private int count;
    private static ChannelGroup clients=new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        //建立连接
        log.info("建立连接client ip:{}",ctx.channel().remoteAddress());
        clients.add(ctx.channel());
    }
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageProtocol msg) throws Exception {
//        System.out.println("type="+(int)msg.getContent()[0]);
        byte []x=new byte[4];
        byte []y=new byte[4];
        System.arraycopy(msg.getContent(),1,x,0,4);
        System.arraycopy(msg.getContent(),5,y,0,4);
//        System.out.println(Utils.bytes2int(x)+":"+Utils.bytes2int(y));
        BlockingQueue<MessageProtocol> bean = SpringUtil.getBean("messageProtocolBlockingQueue", BlockingQueue.class);
        if(bean.size()>Integer.parseInt(cleanSize)){
            bean.clear();
        }
        bean.put(msg);
    }

    @Override
    public void run() {
        BlockingQueue<ByteBuffer> queue = SpringUtil.getBean("byteBuffersBlockingQueue", BlockingQueue.class);
        while (true) {
            try {
                //转发报文
                ByteBuffer take = queue.take();
                take.flip();
                byte []data=new byte[take.limit()];
                take.get(data,0,take.limit());
                for (Channel client : clients) {
                    MessageProtocol messageProtocol = new MessageProtocol();
                    messageProtocol.setLen(take.limit());
                    messageProtocol.setContent(data);
                    client.writeAndFlush(messageProtocol);
                }
                take.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
package com.lbl.server_monitor.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/9/2 10:12
 */
@RestController
@Slf4j
@RequestMapping("/monitor")
public class MonitorMapController {

    @Autowired
    private Map<String, List<String>> monitorMap;

    @RequestMapping("/getMonitorClient")
    public Map<String, List<String>> getMonitorClient(){
        return monitorMap;
    }
}

package com.lbl.server_monitor.udp;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.lbl.server_monitor.maiil.MailSend;
import com.lbl.server_monitor.utils.SendMailUtils;
import com.lbl.server_monitor.utils.SpringUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * @author lbl
 */
@Slf4j
public class ReceivePacketBuilder {
	private static  Map<String, List<String>> monitorMap = SpringUtil.getBean("monitorMap", Map.class);
	private static  SendMailUtils sendMailUtils = SpringUtil.getBean("sendMailUtils", SendMailUtils.class);

	private static  String intervalTime = SpringUtil.getProperties("monitor.client.keep.interval.time");
	private static  String mailTime = SpringUtil.getProperties("monitor.client.sand.mail.time");

	private static  String cleanSize = SpringUtil.getProperties("monitor.view.block.queue.clean.size");
	/**
	 * 60字节
	 */
	private final static int MAX_UNIT=60*1024;
	private DatagramSocket socket;
	private byte[] packages=new byte[14+MAX_UNIT];
	private DatagramPacket p=new DatagramPacket(packages, packages.length);
	public ReceivePacketBuilder(int udpPort) {
		try {
			log.info("udp server start port is {}",udpPort);
			socket=new DatagramSocket(udpPort);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将客户端的数据放入阻塞队列中
	 */
	public void forwardToTcp(){
		try {
			socket.receive(p);
			//设置上线时间
			this.timeOnline();
			//放入阻塞队列
			this.putIntoBlockingQueue();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 接收的数据放入阻塞队列
	 */
	private void putIntoBlockingQueue() throws InterruptedException {
		BlockingQueue<ByteBuffer> queue = SpringUtil.getBean("byteBuffersBlockingQueue", BlockingQueue.class);
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(p.getLength());
		byteBuffer.put(packages,0,p.getLength());
		//清除太久的数据
		if(queue.size()>Integer.parseInt(cleanSize)){
			queue.clear();
		}
		//放入阻塞队列
		queue.put(byteBuffer);
	}

	/**
	 * 上线时间监控
	 */
	private void timeOnline(){
		InetAddress address = p.getAddress();
		String hostAddress = address.getHostAddress();
		List<String> strings = monitorMap.get(hostAddress);
		if(CollUtil.isEmpty(strings)){
			List<String> arr=new ArrayList<>();
			arr.add(DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
			MailSend mailSend = new MailSend();
			mailSend.setContent(hostAddress+"上线感知："+DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
			mailSend.setTopic(hostAddress);
			mailSend.setRecipient("2456709177@qq.com");
			sendMailUtils.sendSimpleMail(mailSend);
			monitorMap.put(hostAddress,arr);
		}else{
			String s = strings.get(strings.size() - 1);
			long between = DateUtil.between(DateUtil.parse(s, "yyyy-MM-dd HH:mm:ss"), new Date(), DateUnit.MINUTE);
			if(between>=Integer.parseInt(intervalTime)){
				strings.add(DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
			}
			if(between>=Integer.parseInt(mailTime)){
				MailSend mailSend = new MailSend();
				mailSend.setContent(hostAddress+"上线感知："+DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
				mailSend.setTopic(hostAddress);
				mailSend.setRecipient("2456709177@qq.com");
				sendMailUtils.sendSimpleMail(mailSend);
			}
		}

	}
}

package com.lbl.server_monitor.udp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author lzm
 * 最大接受6M的数据
 *
 */
public class RobotServerUtils {
	private final static int MAX_UNIT=60*1024;//60字节

	/**
	 * 将byte数组组装成单元
	 * @param packge
	 * @param length
	 * @return
	 */
	public static UnitFrame conversionUnitFrame(byte packge[],int length) {
		UnitFrame frame=new UnitFrame();
		frame.setTimeId(Utils.bytes2Long2(packge));
		frame.setCount((int)packge[8]);
		frame.setFrameNo((int)packge[9]);
		byte []bt=new byte[4];
		System.arraycopy(packge, 10, bt, 0, 4);
		frame.setDataLen(Utils.bytes2int(bt));
		byte[] dataArray =new byte[frame.getDataLen()];
		System.arraycopy(packge, 14, dataArray, 0, length-14);
		frame.setDataArray(dataArray);
		return frame;
	}


	/**
	 * 压缩流
	 * @param bigData
	 * @return
	 */
	public static byte[] zipSendImage(byte []bigData) {
		ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
		ZipOutputStream zipOutputStream=new ZipOutputStream(outputStream);
		try {
			zipOutputStream.putNextEntry(new ZipEntry("dome"));
			zipOutputStream.write(bigData);
			zipOutputStream.close();
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 解压缩流
	 * @param bigData
	 * @return
	 */
	public static byte[] zipReceiveImage(byte []bigData) {
		ByteArrayInputStream outputStream=new ByteArrayInputStream(bigData);

		ByteArrayOutputStream arrayOutputStream=new ByteArrayOutputStream();
		ZipInputStream inputStream=new ZipInputStream(outputStream);
		try {
			inputStream.getNextEntry();
			byte[] buff =new byte[1024];
			int len=0;
			while((len=inputStream.read(buff))!=-1) {
				arrayOutputStream.write(buff,0,len);
			}
			inputStream.close();
			return arrayOutputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * 将图片转换成list单元
	 * @param image
	 * @return
	 */
	public static List<UnitFrame> conversionUnit(BufferedImage image){
		List<UnitFrame> listUnitFrame=new ArrayList<UnitFrame>();
		byte []imageBytes=imageToBytes(image);
		int length = imageBytes.length;
		int unitCount=0;
		Long timeId=System.currentTimeMillis();
		if(length%MAX_UNIT==0) {
			unitCount=length/MAX_UNIT;
		}else {
			unitCount=length/MAX_UNIT+1;
		}
		UnitFrame frame=null;

		for (int i = 0; i < unitCount; i++) {
			frame=new UnitFrame();
			if(unitCount-1==i) {//最后的单元长度
				if(length%MAX_UNIT==0) {//说明刚刚好
					frame.setTimeId(timeId);
					frame.setFrameNo(i);
					frame.setCount(unitCount);
					frame.setDataLen(MAX_UNIT);
					byte[] dataArray=new byte[MAX_UNIT];
					System.arraycopy(imageBytes, i*MAX_UNIT, dataArray, 0, MAX_UNIT);
					frame.setDataArray(dataArray);
				}else {
					frame.setTimeId(timeId);
					frame.setFrameNo(i);
					frame.setCount(unitCount);
					frame.setDataLen(length%MAX_UNIT);
					byte[] dataArray=new byte[length%MAX_UNIT];
					System.arraycopy(imageBytes, i*MAX_UNIT, dataArray, 0, length%MAX_UNIT);
					frame.setDataArray(dataArray);
				}
			}else {//前面的单元
				frame.setTimeId(timeId);
				frame.setFrameNo(i);
				frame.setCount(unitCount);
				frame.setDataLen(MAX_UNIT);
				byte[] dataArray=new byte[MAX_UNIT];
				System.arraycopy(imageBytes, i*MAX_UNIT, dataArray, 0, MAX_UNIT);
				frame.setDataArray(dataArray);
			}
			listUnitFrame.add(frame);
		}
		return listUnitFrame;
	}

	/**
	 * 将图片转换成byte数组
	 * @param image
	 * @return
	 */
	private static byte[] imageToBytes(BufferedImage image) {
		ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
		try {
			ImageIO.write(image, "jpg", outputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return outputStream.toByteArray();
	}
}

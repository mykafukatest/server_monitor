package com.lbl.server_monitor.utils;

import com.lbl.server_monitor.maiil.MailSend;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * @date : 2019/10/22/17:04
 * @desc : 发送邮件的工具类
 */
@Slf4j
@Component
public class SendMailUtils {

    @Autowired
    private JavaMailSender javaMailSender;

    /**
     * 发送一封简单的邮件
     * @param mailSend
     */
    public void sendSimpleMail(MailSend mailSend) {
        try {
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            //邮件发送人
            simpleMailMessage.setFrom(MailSend.SENDER);
            //邮件接收人
            simpleMailMessage.setTo(mailSend.getRecipient());
            //邮件主题
            simpleMailMessage.setSubject(mailSend.getTopic());
            //邮件内容
            simpleMailMessage.setText(mailSend.getContent());
            javaMailSender.send(simpleMailMessage);
        } catch (Exception e) {
            log.error("邮件发送失败{}", e.getMessage());
        }
    }
}

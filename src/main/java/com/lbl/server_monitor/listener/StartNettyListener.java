package com.lbl.server_monitor.listener;

import com.lbl.server_monitor.udp.Monitor;
import com.lbl.server_monitor.utils.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * @author lbl
 * netty随着spring容器加载完毕启动
 */
@Slf4j
@Component
public class StartNettyListener implements ApplicationListener<ContextRefreshedEvent> {

	@Value("${monitor.log.endurance.frequency}")
	private Long frequency;

	@Value("${monitor.upd.server.port}")
	private int udpPort;

	@Value("${monitor.enable}")
	private String monitorEnable;

	@Value("${monitor.tcp.server.port}")
	private int tcpPort;
	@Value("${monitor.upd.tcp.server.port}")
	private int uTcpPort;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if(event.getApplicationContext().getParent()==null) {
			if("udp".equals(monitorEnable)){
				Monitor.startModel1(udpPort,tcpPort);
			}else if("tcp".equals(monitorEnable)){
				Monitor.startModel2(uTcpPort,tcpPort);
			}

//			new Thread(()->{
//				while (true){
//					try {
//						Thread.sleep(frequency);
//						Map<String, List<String>> queue = SpringUtil.getBean("monitorMap", Map.class);
//
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//				}
//			}).start();
		}
	}
}
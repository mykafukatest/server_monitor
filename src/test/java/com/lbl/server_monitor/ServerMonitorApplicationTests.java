package com.lbl.server_monitor;

import com.lbl.server_monitor.maiil.MailSend;
import com.lbl.server_monitor.utils.SendMailUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ServerMonitorApplicationTests {

    @Autowired
    SendMailUtils sendMailUtils;

    @Test
    void contextLoads() {
        MailSend mailSend = new MailSend();
        mailSend.setContent("今晚东莞嫖娼");
        mailSend.setTopic("今晚玩游戏");
        mailSend.setRecipient("2456709177@qq.com");
        sendMailUtils.sendSimpleMail(mailSend);
    }

}

package com.lbl.server_monitor.maiil;

import lombok.Data;

/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/22/17:01
 * @desc : 封装邮件接收者信息
 */
@Data
public class MailSend {

    public static final String SENDER = "2456709177@qq.com";
    /**
     * 邮件的接受者
     */
    private String recipient;
    /**
     * 邮件的主题
     */
    private String topic;
    /**
     * 邮件的内容
     */
    private String content;
}

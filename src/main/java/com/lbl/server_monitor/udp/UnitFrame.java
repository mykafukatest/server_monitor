package com.lbl.server_monitor.udp;

import lombok.Data;

/**
 * @author lbl
 * 报文单元
 */
@Data
public class UnitFrame {
	/**
	 * 8个字节的时间戳
	 * 1个字节的报文总个数
	 * 1个字节的报文编号
	 * 4个字节的报文长度
	 * dataArray[]数据的长度
	 */
	private Long timeId;
	
	private Integer count;
	
	private Integer frameNo;
	
	private Integer dataLen;
	
	private byte[] dataArray;
}
